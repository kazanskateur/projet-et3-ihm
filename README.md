# Obis 3D

## Description
Projet java visant à implementer une represention claire de la population sousmarine

## Avancement
- La partie donnée à un instant t fonctionne parfaitement
    - On peut selectionner un geohash, il s'actualisera en temps réel
    - Quand l'utilisateur tape un nom d'espece, si le nom n'est pas connu une liste de 20 noms apparaitra dans un encart. Si le nom est connu, ils disparaissent de l'encart et les données sont affichées sur le globe
    - La checkbox Histogrammes permet d'afficher ou non les histogrammes, au cours de l'animation ou bien lors de l'affichage des données

- La partie animation fonctionne avec quelque problemes
    - A chaque appui sur le bouton start un nouveau Timer est créé et à chaque interaction avec un timer(demarrage, pause, redemarrage) cela sera appliqué à tout les autres Timers deja créés.
    - L'utilisation des fonctions de données à un instant T s'effectue en parrallele du Timer et ne l'arrete pas.

Une solution aurait ete de declarer un Timer dans la classe et de travailler avec ce dernier. Faute de temps j'ai du me resoudre à abandonner cette idée.

## Installation
Arguments de la VM:
--module-path "C:\Users\kazan\Documents\openjfx-18_windows-x64_bin-sdk\javafx-sdk-18\lib" --add-modules javafx.fxml,javafx.controls

Ne pas oublier d'inclure le jar du dossier deps DANS LE CLASS-PATH et non pas dans le module-path

## Authors
- Hamon Kazan
- Gourion Maxime

## Project status
En devellopement
