package geohash;

import geohash.GeoHashHelper;

public final class Location {
	private final String name;
	private final double latitude;
	private final double longitude;
	
	public Location(String name, double latitude, double longitude) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String name() {
		return name;
	}

	public double lat() {
		return latitude;
	}

	public double lng() {
		return longitude;
	}

	@Override
	public String toString() {
		return "Location [name=" + name + ", latitude=" + latitude + ", longitude=" + longitude +  "]";
	}
	
	//Retourne l'erreur en longitude et latitude d'un geohash
	public static double[] getErreur(int precision) {
		double[] res = new double[2];
		int erreur = precision * 2;
		res[0] = 90/(Math.pow(2,erreur));
		res[1] = 180/(Math.pow(2,erreur));
		return res;
	}
	
	//Retourne l'erreur en longitude et latitude d'un geohash
	public static double[] getErreur(String geoHash) {
		double[] res = new double[2];
		int erreur = geoHash.length() * 2;
		res[0] = 90/(Math.pow(2,erreur));
		res[1] = 180/(Math.pow(2,erreur));
		return res;
	}
	
}