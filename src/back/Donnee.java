package back;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import geohash.Location;

public class Donnee {
	private ArrayList<Espece> espece;
	private String geoHash;
	private int precisionGeoHash;
	private Location bottomLeft;
	private Location bottomRight;
	private Location topRight;
	private Location topLeft;
	
	public Donnee() {
		espece = new ArrayList<Espece>();
	}
	
	//On passe un geohash en parametre, et on renvoi une liste d'espece etant apparu sur ce Geohash avec leurs informations
	public Donnee(String geohash) throws IOException, JSONException {		
		String scientific_name;
		espece = new ArrayList<Espece>();

		String url = "https://api.obis.org/v3/occurrence?geometry="+geohash+"&";
		int Id = 0;
		
		JSONObject infoDonnee = Json.readJsonFromUrl(url);
		JSONArray donn = infoDonnee.getJSONArray("results");//on recupere un array de JSON comportant chaqun des coordones et le nombre d'apparition sur ces points

		
		for(int i = 0; i< donn.length() ;i++) {
			scientific_name = donn.getJSONObject(i).getString("scientificName");
			
			JSONObject infoEspece = Json.readJsonFromUrl("https://api.obis.org/v3/taxon/"+scientific_name);
			JSONArray arr = infoEspece.getJSONArray("results");
			Id = arr.getJSONObject(0).getInt("taxonID");//on recupere le taxonID
			
			Espece e = new Espece(scientific_name,Id);
			try {
				e.setOrder(donn.getJSONObject(i).getString("order"));
			}catch(JSONException err){
				e.setOrder(null);
			}try {
				e.setSuperclass(donn.getJSONObject(i).getString("superclass"));
			}catch(JSONException err){
				e.setSuperclass(null);
			}try {
				e.setRecordedBy(donn.getJSONObject(i).getString("recordedBy"));
			}catch(JSONException err){
				e.setRecordedBy(null);
			}try {
				e.setSpecies(donn.getJSONObject(i).getString("species"));
			}catch(JSONException err){
				e.setSpecies(null);
			}
			espece.add(e);
			
		}
			
	}
	
	
	public void setGeohash(String g) {
		this.geoHash = g;
	}
	
	public void setPrecision(int s) {
		this.precisionGeoHash = s;
	}
	
	public void setEspece(Espece e) {
		this.espece.add(e);
	}
	
	
	public ArrayList<Espece> getEspece() {
		
		return this.espece;
	}
	
	public void setLocationBL(String name, Double lattitude, Double longitude) {
		this.bottomLeft = new Location(name, lattitude, longitude);
	}
	public void setLocationBR(String name, Double lattitude, Double longitude) {
		this.bottomRight = new Location(name, lattitude, longitude);
	}
	public void setLocationTR(String name, Double lattitude, Double longitude) {
		this.topRight = new Location(name, lattitude, longitude);
	}
	public void setLocationTL(String name, Double lattitude, Double longitude) {
		this.topLeft = new Location(name, lattitude, longitude);
	}

	public Location getLocationBL() {
		return this.bottomLeft;
	}
	
	public Location getLoactionBR() {
		return this.bottomRight;
	}
	
	public Location getLocationTR() {
		return this.topRight;
	}
	
	public Location getLocationTL() {
		return this.topLeft;
	}
	public String getGeo() {
		return geoHash;
	}
	
}
