package back;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.awt.Color;
import java.io.*;
import java.net.*;
import java.nio.charset.*;
import org.json.*;

import geohash.GeoHash;
import geohash.GeoHashHelper;
import geohash.Location;


public class DonneeMonde {
	private Hashtable<String,Donnee> donnees_monde;
	int max;
	
	
	
	
	//on prend un scientifique name, une precision Geohash, une startDate, une endDate et on cree les donnees correspondantes sous la forme d'une ArrayList
		public DonneeMonde(String scientific_name, int precisionGeohash) throws JSONException {		
			try {
			this.donnees_monde = new Hashtable<String,Donnee>();
			
			
			String url = "https://api.obis.org/v3/occurrence/grid/"+precisionGeohash+"?";
			int Id = 0;
			if(scientific_name != null) {
				scientific_name.replaceAll(" ", "%20");
				url = url + "scientificname="+scientific_name+"&";
				JSONObject infoEspece;
				
					infoEspece = Json.readJsonFromUrl("https://api.obis.org/v3/taxon/"+scientific_name);
				
				JSONArray arr = infoEspece.getJSONArray("results");
				Id = arr.getJSONObject(0).getInt("taxonID");//on recupere le taxonID
				System.out.println("URL: " + url);
			}
			
			
			
			
			JSONObject infoDonnee = Json.readJsonFromUrl(url);
			JSONArray donn = infoDonnee.getJSONArray("features");//on recupere un array de JSON comportant chaqun des coordonees et le nombre d'apparition sur ces points
			

			
			for(int i = 0; i< donn.length() ;i++) {
				Donnee donnee = new Donnee();
				
				donnee.setPrecision(precisionGeohash);
				Espece e = new Espece(scientific_name,Id);
				
				e.setQuantite(donn.getJSONObject(i).getJSONObject("properties").getInt("n"));//on ajoute le nombre d'apparition a la donnee
				if(e.getQuantite()>this.max) {
					this.max = e.getQuantite();
				}
				double long1 = donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(0).getDouble(0);
				double latt1 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(0).getDouble(1);
				double long2 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(1).getDouble(0);
				double latt2 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(1).getDouble(1);
				double long3 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(2).getDouble(0);
				double latt3 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(2).getDouble(1);
				double long4 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(3).getDouble(0);
				double latt4 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(3).getDouble(1);
				String geohash = GeoHashHelper.getGeohash(new Location("point",(latt3+latt1)/2,(long3+long1)/2), precisionGeohash);
				donnee.setGeohash(geohash);
				donnee.setLocationBL("BottomLeft", latt1, long1);
				donnee.setLocationBR("BottomRight", latt2, long2);
				donnee.setLocationTR("TopRight", latt3, long3);
				donnee.setLocationTL("TopLeft", latt4, long4);
				
				
				donnee.setEspece(e);//on cree l'espece
				this.donnees_monde.put(geohash, donnee);
				} 
			} catch (IOException e) {
				
			}
			
		}
		
		
		public Hashtable<String,Donnee> getHashtable(){
			return this.donnees_monde;
		}
	
	
		public DonneeMonde(String scientific_name, int precisionGeohash, String startDate, String endDate) {		
			try {
			this.donnees_monde = new Hashtable<String,Donnee>();
			
			
			String url = "https://api.obis.org/v3/occurrence/grid/"+precisionGeohash+"?startdate="+startDate+"&enddate="+endDate+"&";
			int Id = 0;
			if(scientific_name != null) {
				scientific_name.replaceAll(" ", "%20");
				url = url + "scietificname="+scientific_name+"&";
				JSONObject infoEspece = Json.readJsonFromUrl("https://api.obis.org/v3/taxon/"+scientific_name);
				JSONArray arr = infoEspece.getJSONArray("results");
				Id = arr.getJSONObject(0).getInt("taxonID");//on recupere le taxonID
			}
			
			
			
			
			JSONObject infoDonnee = Json.readJsonFromUrl(url);
			JSONArray donn = infoDonnee.getJSONArray("features");//on recupere un array de JSON comportant chaqun des coordonees et le nombre d'apparition sur ces points
			
			for(int i = 0; i< donn.length() ;i++) {
				Donnee donnee = new Donnee();
				Espece e = new Espece(scientific_name,Id);
				
				e.setQuantite(donn.getJSONObject(i).getJSONObject("properties").getInt("n"));//on ajoute le nombre d'apparition a la donnee
				if(e.getQuantite()>this.max) {
					this.max = e.getQuantite();
				}
				double long1 = donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(0).getDouble(0);
				double latt1 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(0).getDouble(1);
				double long2 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(1).getDouble(0);
				double latt2 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(1).getDouble(1);
				double long3 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(2).getDouble(0);
				double latt3 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(2).getDouble(1);
				double long4 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(3).getDouble(0);
				double latt4 =  donn.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0).getJSONArray(3).getDouble(1);
				String geohash = GeoHashHelper.getGeohash(new Location("point",(latt3+latt1)/2,(long3+long1)/2), precisionGeohash);
				donnee.setGeohash(geohash);
				
				donnee.setLocationBL("BottomLeft", latt1, long1);
				donnee.setLocationBR("BottomRight", latt2, long2);
				donnee.setLocationTR("TopRight", latt3, long3);
				donnee.setLocationTL("TopLeft", latt4, long4);
				
				donnee.setEspece(e);//on cree l'espece
				this.donnees_monde.put(geohash, donnee);
				} 
				
			
				
			}
			
			catch (JSONException | IOException e) {
				
				e.printStackTrace();
			}
			
		}
		
		
		
		public int getMax(){
			return this.max;
		}
		
	
	
}

