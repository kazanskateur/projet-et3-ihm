package back;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import org.json.JSONException;

import view.Controller;

public class Model {
	private Controller controller;
	
	public Model(Controller control) {
		this.controller = control;
	}
	
	public void showEspeces(String name, int precision, boolean histo){
		DonneeMonde data;
		try {
			data = new DonneeMonde(name,precision);
			//On recupere les donnees
			Hashtable<String, Donnee> listData = data.getHashtable();
			int max = data.getMax();
			//Pour chaque geoHash
	        for (String key : listData.keySet()) {
	        	Donnee donnee = listData.get(key);
	        	ArrayList<Espece> especes = donnee.getEspece();
	        	//Si il y'a plus d'une espece on cancel
	        	if(especes.size() > 1) {
	        		controller.updateInfo("Erreur dans les especes collectees");
	        	}
	        	//Sinon on affiche tous les geohash
	        	else {
	        		int nombre = especes.get(0).getQuantite();
	        		controller.drawQuadriHash(donnee.getLocationTL(),donnee.getLocationTR(),donnee.getLoactionBR(),donnee.getLocationBL(),nombre, max);
	        		if(histo) {
	        			controller.drawHisto(donnee.getGeo(), nombre, max);
	        		}
	        		controller.updateInfo("Espece existante");
	        	}
	        		
	         }
		} 
		//L'espece n'existe pas
		catch (JSONException e) {
			controller.updateInfo("Espece inexistante");
		}
		
		
	}

	public void timeLaps(String name, Date start, Date end, int intervalle, int precision) {
		try {
			TimeLaps timeLaps = new TimeLaps(name, precision,start,end,intervalle);
			controller.animation(timeLaps,intervalle);
			
		} catch (IOException | JSONException e) {
				controller.updateError("Especes inconnus: Nous ne pouvons demarrer l'animation");
		}
		
	}
	
	public void giveInfoGeohash(String geohash) {
		try {
			Donnee data = new Donnee(geohash);
			controller.popUpEspeces(geohash,data.getEspece());
			
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		
	}

	public void progres(float progres) {
		controller.progres(progres);
	}
	
	//On prend un debut de nom, et on retourne les premier scientificName commencant par cette chaine, reparti sur 2 listes
	public ArrayList<String> autocomplete(String name) throws IOException, JSONException {
		ArrayList<String> list = Json.trouverEspece(name);
		ArrayList<String> listphrase = new ArrayList<String>();
		String phrase = "";
		String phrase2="";
		for(int i =0;i<list.size()/2;i++) {
			phrase = phrase + list.get(i) + "\n";
		}for(int i = list.size()/2;i<list.size();i++) {
			phrase2 = phrase2 + list.get(i)+"\n";
		}
		
		listphrase.add(phrase);
		listphrase.add(phrase2);
		return listphrase;
	}
}
