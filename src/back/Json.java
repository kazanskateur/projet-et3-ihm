package back;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Json {

	//on prend un Url sous la forme d'un String en parametre
	public static JSONArray readJsonAFromUrl(String Url) throws IOException, JSONException {
	    InputStream is = new URL(Url).openStream();
	    try {
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
	      String jsonText = readAll(rd);
	      JSONArray json = new JSONArray(jsonText);
	      return json;//on return un JSONArray (commence par [)
	    } finally {
	      is.close();
	    }
	  }
	
	//on prend un Url sous forme d'un String en parametre
	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
	    InputStream is = new URL(url).openStream();
	    try {
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
	      String jsonText = readAll(rd);
	      JSONObject json = new JSONObject(jsonText);
	      return json;//on return un JSONObject (commence par {)
	    } finally {
	      is.close();
	    }
	  }
	
	//on prend un scientificName en parametre
	public static ArrayList<String> trouverEspece(String scientificName) throws IOException, JSONException {
		scientificName.replaceAll(" ", "%20");
		JSONArray listeEspece = Json.readJsonAFromUrl("https://api.obis.org/v3/taxon/complete/verbose/"+scientificName);//on charge la liste dans un JSONArray
		ArrayList<String> liste = new ArrayList<String>();
		
		
		for(int i = 0;i<listeEspece.length();i++) {
			String nom = listeEspece.getJSONObject(i).getString("scientificName");//on recupere le nom pour chaque objet de la liste
			liste.add(nom);
		}
		return liste;
		
	}
	
	
	private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }
}
