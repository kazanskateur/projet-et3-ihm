package back;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;

public class TimeLaps {
	private ArrayList<DonneeMonde> donneesTime = new ArrayList<DonneeMonde>();
	

	
	public TimeLaps(String scientific_name, int precisionGeohash,Date startDate,Date endDate,int nbIntervalle) throws IOException, JSONException {
		
		SimpleDateFormat formater = new SimpleDateFormat("YYYY-MM-dd");
		scientific_name.replaceAll(" ", "%20");
		
		donneesTime = new ArrayList<DonneeMonde>();
		long intervalle =((endDate.getTime()-startDate.getTime())/nbIntervalle);
		

		Date dateIntervalleStart= new Date(startDate.getTime());
		Date dateIntervalleEnd =new Date(startDate.getTime()+intervalle);
		while(dateIntervalleStart.before(endDate)) {
			donneesTime.add(new DonneeMonde(scientific_name,precisionGeohash,formater.format(dateIntervalleStart),formater.format(dateIntervalleEnd)));
			dateIntervalleStart = dateIntervalleEnd;
			dateIntervalleEnd = new Date(dateIntervalleStart.getTime()+intervalle);
		}
		
	}
	
	public TimeLaps(String scientific_name, int precisionGeohash,Date startDate,Date endDate,int nbIntervalle, Model model) throws IOException, JSONException {
		
		SimpleDateFormat formater = new SimpleDateFormat("YYYY-MM-dd");
		scientific_name.replaceAll(" ", "%20");
		
		donneesTime = new ArrayList<DonneeMonde>();
		long intervalle =((endDate.getTime()-startDate.getTime())/nbIntervalle);
		

		Date dateIntervalleStart= new Date(startDate.getTime());
		Date dateIntervalleEnd =new Date(startDate.getTime()+intervalle);
		int i = 0;
		while(dateIntervalleStart.before(endDate)) {
			donneesTime.add(new DonneeMonde(scientific_name,precisionGeohash,formater.format(dateIntervalleStart),formater.format(dateIntervalleEnd)));
			//model.progres(i*1.0f/nbIntervalle);
			dateIntervalleStart = dateIntervalleEnd;
			dateIntervalleEnd = new Date(dateIntervalleStart.getTime()+intervalle);
		}
		
	}
	
	
	public TimeLaps(int precisionGeohash,Date startDate,Date endDate,int nbIntervalle) throws IOException, JSONException {
			
			SimpleDateFormat formater = new SimpleDateFormat("YYYY-MM-DD");
			
			donneesTime = new ArrayList<DonneeMonde>();
			long intervalle =((endDate.getTime()-startDate.getTime())/nbIntervalle);
			
			TimeUnit time = TimeUnit.DAYS;
			//intervalle = time.convert(intervalle, TimeUnit.MILLISECONDS);
			
			Date dateIntervalleStart= new Date(startDate.getTime());
			Date dateIntervalleEnd =new Date(startDate.getTime()+intervalle);
			while(dateIntervalleStart!=endDate) {
				donneesTime.add( new DonneeMonde(null,precisionGeohash,formater.format(dateIntervalleStart),formater.format(dateIntervalleEnd)));
				dateIntervalleStart= dateIntervalleEnd;
				dateIntervalleEnd = new Date(startDate.getTime()+intervalle);
				System.out.println(startDate.toString());
			}
			
		}
		
		public ArrayList <DonneeMonde> getData(){
			return donneesTime;
		}
	
	
}

