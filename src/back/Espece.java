package back;


public class Espece {
	private String ScientificName;
	private int Id;
	private int quantite;
	private String order;
	private String superclass;
	private String recordedBy;
	private String species;
	
	public Espece(String s, int id) {
		this.ScientificName = s;
		this.Id = id;
	}
	@Override
	public String toString() {
		String res = "";
		res += "Espece: " + ScientificName;
		res += "\nOrder: " + order;
		res += "\nSuperclass: " + superclass;
		res += "\nRecorded by: " + recordedBy;
		res += "\nSpecies: " + species;
		return res;
		
	}
	
	public void setScientificName(String s) {
		this.ScientificName = s;
	}
	
	public void setID(int i) {
		this.Id = i;
	}
	
	public void setQuantite(int q) {
		this.quantite = q;
	}
	
	public void setOrder(String o) {
		this.order = o;
	}
	
	public void setSuperclass(String o) {
		this.superclass = o;
	}
	
	public void setRecordedBy(String o) {
		this.recordedBy = o;
	}
	
	public void setSpecies(String o) {
		this.species = o;
	}
	
	public String getOrder() {
		return this.order;
	}
	
	public String getSuperclass() {
		return this.superclass;
	}
	
	public String getRecordedBy() {
		return this.recordedBy;
	}
	
	public String getSpecies() {
		return this.species;
	}
	
	public int getQuantite() {
		return this.quantite;
	}
	
	public int getID() {
		return this.Id;
	}
	
	public String getName() {
		return this.ScientificName;
	}
	
}
