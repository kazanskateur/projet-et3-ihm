package view;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.ResourceBundle;


import org.json.JSONException;

import com.interactivemesh.jfx.importer.ImportException;
import com.interactivemesh.jfx.importer.obj.ObjModelImporter;

import back.Donnee;
import back.DonneeMonde;
import back.Espece;
import back.Model;
import back.TimeLaps;
import geohash.GeoHashHelper;
import geohash.Location;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.geometry.Pos;
import javafx.scene.AmbientLight;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.Sphere;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.stage.Popup;


public class Controller implements Initializable {
	//Pane de la vue 3D
	@FXML
	private Pane pane3D;

	//Entree especes
	@FXML
	private TextField tfNom;
	@FXML
	private Label infoLabel;
	@FXML
	private ComboBox<String> comboGeohash;
	
	//Info selection du geohash
	@FXML
	private Label infoEspece1;
	@FXML
	private Label infoEspece2;
	@FXML
	private CheckBox chkHisto;
	
	//Entree animation
	@FXML
	private CheckBox chkDate;
	@FXML
	private DatePicker pickStart;
	@FXML
	private DatePicker pickEnd;
	@FXML
	private Spinner<Integer> slctInter;
	@FXML
	private Button btnStart;
	@FXML
	private Button btnStop;
	@FXML
	private Button btnRestart;
	@FXML
	private ProgressBar progress;
	
	//Group et scene de la vue 3D
	private Group earth;
	private Group pin;
	private Group root3D;
	private SubScene subscene;
	
	
	private Model model;
	private boolean histo = false;
	
    private static final float TEXTURE_LAT_OFFSET = -0.2f;
    private static final float TEXTURE_LON_OFFSET = 2.8f;
    private static final float TEXTURE_OFFSET = 1.01f;
    
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
        //INITIALISATION DES INPUTS
        tfNom.setText("Delphinidae");
    	comboGeohash.getSelectionModel().select(2);
    	pickStart.setValue(LocalDate.of(1970,6,23));
    	pickEnd.setValue(LocalDate.now());
    	
		//Creation du model
	    model = new Model(this);
	
		//Create a Pane and graph scene root for the 3D content
	    root3D = new Group();
		//Add a camera group
		PerspectiveCamera camera = new PerspectiveCamera(true);
		
		// Create the subscene
		subscene = new SubScene(root3D, 750, 750, true, SceneAntialiasing.BALANCED);
		subscene.setCamera(camera);
		subscene.setFill(Color.GREY);
		pane3D.getChildren().addAll(subscene);
		
        // Load geometry
        ObjModelImporter objImporter = new ObjModelImporter();
        try {
        	URL modelUrl = this.getClass().getResource("Earth/earth.obj");
        	objImporter.read(modelUrl);
        }catch(ImportException e) {
        	System.out.println(e.getMessage());
        }
        MeshView[] meshViews = objImporter.getImport();
        earth = new Group(meshViews);
        pin = new Group();
        root3D.getChildren().add(earth);
        root3D.getChildren().add(pin);
        
        // Add ambient light
        AmbientLight ambientLight = new AmbientLight(Color.WHITE);
        ambientLight.getScope().addAll(root3D);
        root3D.getChildren().add(ambientLight);

        //Scaling de la terre pr voir le geohash
        earth.setScaleX(0.99);
        earth.setScaleY(0.99);
        earth.setScaleZ(0.99);

        //Handler clic planete
		subscene.addEventHandler(MouseEvent.ANY, event -> {
            if(event.getEventType() == MouseEvent.MOUSE_PRESSED && event.isControlDown()) {
                PickResult pickResult = event.getPickResult();
                Point3D spaceCoord = pickResult.getIntersectedPoint();
           
                //On recupere les coordonnees
                Point2D world = SpaceCoordToGeoCoord(spaceCoord);
                Location loc = new Location("geoHash", world.getX(), world.getY());
                //Geohash de la position
                String geoHash = GeoHashHelper.getGeohash(loc,Integer.parseInt(comboGeohash.getValue().toString()));
                //Fonction affichant les infos dans la popup
                model.giveInfoGeohash(geoHash);

                    
            }
        });
		
		
		
		//Listener changement de texte
        tfNom.textProperty().addListener((observable, oldValue, newValue) ->{
        	//On clear la planete
        	pin.getChildren().clear();
        	//On montre l'espece entree
        	model.showEspeces(newValue,Integer.parseInt(comboGeohash.getValue().toString()), histo);
        	//On essaie de montrer les especes qui commencent par la chaine
        	try {
				infoEspece1.setText(model.autocomplete(newValue).get(0));
				infoEspece2.setText(model.autocomplete(newValue).get(1));
			} catch (IOException | JSONException e) {
				e.printStackTrace();
			}
        });
		
        
        //Listener changement de geohash
        comboGeohash.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
        		//On nettoie puis on reaffiche
        		pin.getChildren().clear();
        		model.showEspeces(tfNom.getText(),Integer.parseInt(comboGeohash.getValue().toString()),histo);
        	});
        
        //Bouton de changement d'etat de date
        chkDate.selectedProperty().addListener((observable, oldValue, newValue) -> {
	         if(newValue) {
	        	 enableDate();
	         }
	         else {
	        	 disableDate();
	         }
	      });
        
        chkHisto.selectedProperty().addListener((observable, oldValue, newValue) -> {
	         if(newValue) {
	        	 histo = true;
	         }
	         else {
	        	 histo = false;
	         }
	         pin.getChildren().clear();
	         model.showEspeces(tfNom.getText(),Integer.parseInt(comboGeohash.getValue().toString()), histo);
	      });
        
        
        btnStart.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	LocalDate localStart = pickStart.getValue();
            	Instant instant1 = Instant.from(localStart.atStartOfDay(ZoneId.systemDefault()));
            	Date start = Date.from(instant1);
            	LocalDate localEnd = pickEnd.getValue();
            	Instant instant = Instant.from(localEnd.atStartOfDay(ZoneId.systemDefault()));
            	Date end = Date.from(instant);
            	//Si la fin est bien apres le debut
            	if(end.after(start)) {
            		infoEspece1.setText("Vos dates sont valides");
            		//On lance l'animation
            		model.timeLaps(tfNom.getText(),start, end, slctInter.getValue(), Integer.parseInt((String) comboGeohash.getValue()));
            	}
            	else if(end.before(start)) {
            		infoEspece1.setText("Vos dates sont invalides");
            	}
            	
            }
        });
        //On desactive la date au demarrage et on montre les dauphins pr un geoHash de 3
        disableDate();
        model.showEspeces("Delphinidae",3,histo);
		new CameraManager(camera, pane3D, root3D);
		
	}
	
	
	//UPDATE FROM MODEL
	public void updateInfo(String info) {
		infoLabel.setText(info);
	}
	public void updateError(String error) {
		infoEspece1.setText(error);
	}
	//On desactive toute les composantes de dates
	public void disableDate() {
		pickStart.setDisable(true);
		pickEnd.setDisable(true);
		slctInter.setDisable(true);
		btnStart.setDisable(true);
		btnStop.setDisable(true);
		btnRestart.setDisable(true);
		progress.setDisable(true);
		
	}
	//On active toute les composantes de dates
	public void enableDate() {
		pickStart.setDisable(false);
		pickEnd.setDisable(false);
		slctInter.setDisable(false);
		btnStart.setDisable(false);
		btnStop.setDisable(false);
		btnRestart.setDisable(false);
		progress.setDisable(false);
	}
	
	
	//Ajoute une tuile qui couvre les 4 localisations fournies en fonction du nombre d'especes et de leurs max
	public void drawQuadriHash(Location topRi, Location topLe, Location botLe, Location botRi, int nbIndiv, int max) {
		//On convertit en Point3D
        Point3D botL = geoCoordTo3dCoord((float)botLe.lat(),(float)botLe.lng());
        Point3D topL = geoCoordTo3dCoord((float)topLe.lat(),(float)topLe.lng());
        Point3D botR = geoCoordTo3dCoord((float)botRi.lat(),(float)botRi.lng());
        Point3D topR = geoCoordTo3dCoord((float)topRi.lat(),(float)topRi.lng());
        //On traduit le nb d'individu en couleur en fonction du max
        Color color = getColor(nbIndiv,max);
		PhongMaterial red = new PhongMaterial(color);
		//Et on ajoute la tuile
		AddQuadrilateral(pin,topL,botL,botR ,topR ,red);
	}
	//Ajoute un histogramme qui couvre les 4 localisations fournies en fonction du nombre d'especes et de leurs max
	public void drawHisto(String geoHash, int nbEspece, int max) {
        Location geoPos = GeoHashHelper.getLocation(geoHash);
        Point3D geoPoint = geoCoordTo3dCoord((float)geoPos.lat(),(float)geoPos.lng());
        //Fonction mathematiques piqu  Matiu
        Point3D end = geoPoint.multiply(1 + (Math.log(nbEspece) / Math.log(1.5)) * 0.02);
        //On affiche le cylindre depuis le centre jusqu' un point au dessus de la planete
        Cylinder cylindre = createLine( new Point3D(0,0,0) ,  end );
        cylindre.setMaterial(new PhongMaterial(getColor(nbEspece,max)));
        pin.getChildren().add(cylindre);
	}
	
	public void popUpEspeces(String geohash,ArrayList<Espece> especes) {
		Popup popup = new Popup();
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.CENTER);

		vbox.setStyle("-fx-background-color: white;");
        String strLabel = "===GeoHash: " + geohash + "===\n\n";
		for(Espece espece : especes) {
			strLabel += espece + "\n\n";
		}
		//On cree le bouton de fermeture
		Button close = new Button("Close");
		close.setOnMouseClicked(event ->  {
			popup.hide();
				});
		Label label = new Label(strLabel);
		vbox.getChildren().add(label);
		vbox.getChildren().add(close);
		popup.getContent().add(vbox);
		popup.show(root3D,0,0);
	}
	
	//Fonction d'animation
	public void animation(TimeLaps timeLaps, int intervalle) {
		final long startNanoTime = System.nanoTime();
		AnimationTimer timer = new AnimationTimer() {
			int pas=0;
 			ArrayList<DonneeMonde> listData = timeLaps.getData();
			public void handle(long currentNanoTime) {
				//On nettoie la planete
				pin.getChildren().clear();
				//On fait la diff entre l'heure de lancement et l'actuelle
				double t = (currentNanoTime - startNanoTime) / 1000000000.0;
				//Si on a fait toute les cases du tableau on recommence l'animation
	     		if(pas == listData.size()-1) {
	     			pas=0;
	     		}
	     		//Sinon si 4 secondes se sont ecoules
	     		else if(t>4) {
	     			//On nettoie
	     			pin.getChildren().clear();
	     			DonneeMonde data = listData.get(pas);
	     			//On recupre la donne pour un instant t
	     			Hashtable<String, Donnee> listDonnee = data.getHashtable();
	     			int max = data.getMax();
	    	        for (String key : listDonnee.keySet()) {
	    	        	Donnee donnee = listDonnee.get(key);
	    	        	ArrayList<Espece> especes = donnee.getEspece();
	    	        	if(especes.size() > 1) {   	        		
	    	        		updateInfo("Erreur dans les especes collectees");
	    	        	}
	    	        	else {
	    	        		int nombre = especes.get(0).getQuantite();
	    	        		drawQuadriHash(donnee.getLocationTL(),donnee.getLocationTR(),donnee.getLoactionBR(),donnee.getLocationBL(),nombre, max);
	    	        		if(histo) {
	    	        			drawHisto(donnee.getGeo(),nombre,max);
	    	        		}
	    	        		updateInfo("Espece existante");
	    	        	}
	    	        	
	    	         }
	    	        progress.setProgress(pas*1.0f/listData.size());
	     			t = (System.nanoTime() - currentNanoTime) / 1000000000.0;
	     			pas +=1;
	     		}
			}
			
		};
		
		//On demarre le timer
		timer.start();
		//On bind l'arret du timer
		btnStop.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	System.out.println("Stop");
            	timer.stop();
            }
        });
		//On bind le redemmarrage du timer
		btnRestart.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	System.out.println("Restart");
            	timer.start();
            }
        });

	}

	//Affichage de la progress bar
	public void progres(float progres) {
		progress.setProgress(progres);
	}
	


	//CONVERSION FONCTION
	public static Point3D geoCoordTo3dCoord(float lat , float lon) {
        float lat_cor = lat + TEXTURE_LAT_OFFSET;
        float lon_cor = lon + TEXTURE_LON_OFFSET;
        return new Point3D(
                -java.lang.Math.sin(java.lang.Math.toRadians(lon_cor))
                        * java.lang.Math.cos(java.lang.Math.toRadians(lat_cor)),
                -java.lang.Math.sin(java.lang.Math.toRadians(lat_cor)),
                java.lang.Math.cos(java.lang.Math.toRadians(lon_cor))
                        * java.lang.Math.cos(java.lang.Math.toRadians(lat_cor)));
    }

    public static Point3D geoCoordTo3dCoord(float lat, float lon, float radius) {
        float lat_cor = lat + TEXTURE_LAT_OFFSET;
        float lon_cor = lon + TEXTURE_LON_OFFSET;
        return new Point3D(
                -java.lang.Math.sin(java.lang.Math.toRadians(lon_cor))
                        * java.lang.Math.cos(java.lang.Math.toRadians(lat_cor))*radius,
                -java.lang.Math.sin(java.lang.Math.toRadians(lat_cor))*radius,
                java.lang.Math.cos(java.lang.Math.toRadians(lon_cor))
                        * java.lang.Math.cos(java.lang.Math.toRadians(lat_cor))*radius);
    }
    
    public static Point2D SpaceCoordToGeoCoord(Point3D p) {

   	 float lat = (float) (Math.asin(-p.getY() / TEXTURE_OFFSET)
   	 * (180 / Math.PI) - TEXTURE_LAT_OFFSET);
   	 float lon;

   	 if (p.getZ() < 0) {
   	 lon = 180 - (float) (Math.asin(-p.getX() / (TEXTURE_OFFSET
   	 * Math.cos((Math.PI / 180)
   	 * (lat + TEXTURE_LAT_OFFSET)))) * 180 / Math.PI + TEXTURE_LON_OFFSET);
   	 } else {
   	 lon = (float) (Math.asin(-p.getX() / (TEXTURE_OFFSET * Math.cos((Math.PI / 180)
   	 * (lat + TEXTURE_LAT_OFFSET)))) * 180 / Math.PI - TEXTURE_LON_OFFSET);
   	 }

   	 return new Point2D(lat, lon);
   	}

    
    //RENDERING FONCTION
    public void displayTown(String name, float latitude, float longitude) {
    	Point3D Seoul = geoCoordTo3dCoord(latitude, longitude, 1.0f);
    	Sphere sphere = new Sphere(0.01);
    	sphere.setTranslateX(Seoul.getX());
    	sphere.setTranslateY(Seoul.getY());
    	sphere.setTranslateZ(Seoul.getZ());

    	earth.getChildren().add(sphere);
    }
    
    private void AddQuadrilateral(Group parent, Point3D topRight, Point3D bottomRight, Point3D bottomLeft, Point3D topLeft, PhongMaterial material) {
    	final TriangleMesh triangleMesh = new TriangleMesh();
    	final float[] points = {
    			(float)topRight.getX(),(float)topRight.getY(),(float)topRight.getZ(),
    			(float)topLeft.getX(),(float)topLeft.getY(),(float)topLeft.getZ(),
    			(float)bottomLeft.getX(),(float)bottomLeft.getY(),(float)bottomLeft.getZ(),
    			(float)bottomRight.getX(),(float)bottomRight.getY(),(float)bottomRight.getZ()
    	};
    	final float[] texCoords = {
    			1, 1,
    			1, 0,
    			0, 1,
    			0, 0
    	};
    	final int[] faces = {
    			0, 1, 1, 0, 2, 2,
    			0, 1, 2, 2, 3, 3
    	};
    	triangleMesh.getPoints().setAll(points);
    	triangleMesh.getTexCoords().setAll(texCoords);
    	triangleMesh.getFaces().setAll(faces);
    	
    	final MeshView meshView = new MeshView(triangleMesh);
    	meshView.setMaterial(material);
    	parent.getChildren().addAll(meshView);
    	
    }
    
    // From Rahel Lthy : https://netzwerg.ch/blog/2015/03/22/javafx-3d-line/
    public Cylinder createLine(Point3D origin, Point3D target) {
        Point3D yAxis = new Point3D(0, 1, 0);
        Point3D diff = target.subtract(origin);
        double height = diff.magnitude();

        Point3D mid = target.midpoint(origin);
        Translate moveToMidpoint = new Translate(mid.getX(), mid.getY(), mid.getZ());

        Point3D axisOfRotation = diff.crossProduct(yAxis);
        double angle = Math.acos(diff.normalize().dotProduct(yAxis));
        Rotate rotateAroundCenter = new Rotate(-Math.toDegrees(angle), axisOfRotation);

        Cylinder line = new Cylinder(0.01f, height);

        line.getTransforms().addAll(moveToMidpoint, rotateAroundCenter);

        return line;
    }
    

    //Conversion de la couleur depuis une quantite en fonction de son max
	public static Color getColor(int quantite, int max) {
		float q = quantite*1.0f;
		float m = max*1.0f;
		
		Color c = Color.rgb(255,(int) (150*(1-q/m)),0,0/255);
		return c;
	}




}
