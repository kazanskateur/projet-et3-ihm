package view;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class InterfaceGUI extends Application{
	@Override
	public void start(Stage primaryStage) {
		try {
			System.out.println(getClass().getResource("fxml/gui2D.fxml"));
			Parent content = FXMLLoader.load(getClass().getResource("fxml/gui2D.fxml"));
			primaryStage.setTitle("App de merde");
			primaryStage.setScene(new Scene(content));
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
